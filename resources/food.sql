CREATE TABLE "foods" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL,
  "kcal" int NOT NULL,
  "carbohydrates" int NOT NULL,
  "sugar" int NOT NULL,
  "salt" int NOT NULL,
  "fat" int NOT NULL,
  "protein" int NOT NULL
);

CREATE TABLE "whitelists" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int NOT NULL,
  combination int[] NOT NULL
);

CREATE TABLE "rules" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int NOT NULL,
  "name" varchar NOT NULL,
  "parameter" json NOT NULL
);

CREATE TABLE "blacklists" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int NOT NULL,
  "combination" int[] NOT NULL
);

CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "username" varchar UNIQUE NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "password" varchar NOT NULL,
  "daily_kcal" int,
  "api_key" varchar,
  "api_key_expire" timestamp
);

CREATE TABLE "stocks" (
  "food_id" int,
  "user_id" int,
  "amount" int,
  PRIMARY KEY ("food_id", "user_id")
);

CREATE TABLE "categories" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL
);

CREATE TABLE "food_category" (
  "food_id" int,
  "category_id" int,
  PRIMARY KEY ("food_id", "category_id")
);

ALTER TABLE "whitelists" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "rules" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "blacklists" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "stocks" ADD FOREIGN KEY ("food_id") REFERENCES "foods" ("id");

ALTER TABLE "stocks" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "food_category" ADD FOREIGN KEY ("food_id") REFERENCES "foods" ("id");

ALTER TABLE "food_category" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");


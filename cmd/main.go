package main

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/rule/ruleManager"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers"
)

func main() {
	dbPool, err := pgxpool.Connect(context.TODO(), helpers.DBUrl)
	if err != nil {
		panic(err)
	}
	defer dbPool.Close()

	test := ruleManager.FoodRuleManager{}

	mip, err := test.ToJson()

	fmt.Println(mip, err)

	return
}

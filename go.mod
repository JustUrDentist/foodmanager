module gitlab.com/JustUrDentist/foodmanager

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/jackc/pgx/v4 v4.9.0
)

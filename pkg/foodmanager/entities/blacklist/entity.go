package blacklist

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers/slice"
	"strconv"
)

type Blacklist struct {
	ID          int
	UserID      int
	Combination []int
}

func (bl *Blacklist) toSQLString() string {
	return "(" + strconv.Itoa(bl.UserID) + ", " + slice.IntToSQLString(bl.Combination) + ")"
}

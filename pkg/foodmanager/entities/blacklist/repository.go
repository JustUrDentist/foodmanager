package blacklist

type Reader interface {
	AllUserBlacklists(uid int) ([]Blacklist, error)
}

type Writer interface {
	InsertUserBlacklist(bl Blacklist) error
	InsertUserBlacklists(bls []Blacklist) error
}

type Repository interface {
	Reader
	Writer
}

package blacklist

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers/typeString"
)

const (
	getBlacklists    = "SELECT * FROM blacklists WHERE user_id = %d"
	insertBlacklists = "INSERT INTO blacklists (user_id, combinatinons) VALUES %s"
)

type Postgres struct {
	Pool *pgxpool.Pool
}

func NewPostgresRepository(p *pgxpool.Pool) Repository {
	return &Postgres{
		Pool: p,
	}
}

func (p *Postgres) AllUserBlacklists(uid int) ([]Blacklist, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var rows pgx.Rows
	var result []Blacklist
	var blacklist Blacklist

	db.Begin()

	rows, db.Err = db.Conn.Query(context.TODO(), db.Queryf(getBlacklists, uid))

	if db.Err != nil {
		db.Rollback()
		return nil, db.Err
	}

	for rows.Next() {
		db.Err = rows.Scan(&blacklist.ID, &blacklist.UserID, &blacklist.Combination)
		if db.Err != nil {
			db.Rollback()
			return nil, db.Err
		}

		result = append(result, blacklist)
	}
	db.Commit()

	return result, nil
}

func (p *Postgres) InsertUserBlacklist(bl Blacklist) error {
	db := helpers.NewDatabaseHelper(p.Pool)

	db.Inserter(db.Queryf(insertBlacklists, bl.toSQLString()))

	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()

	return nil
}

func (p *Postgres) InsertUserBlacklists(bls []Blacklist) error {
	db := helpers.NewDatabaseHelper(p.Pool)
	var values string

	for _, bl := range bls {
		values += bl.toSQLString() + ", "
	}

	typeString.RemoveLastPattern(", ", &values)

	db.Inserter(db.Queryf(insertBlacklists, values))

	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()

	return nil
}

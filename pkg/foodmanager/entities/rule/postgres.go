package rule

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers/typeString"
)

const (
	getAllRules = "SELECT * FROM rules WHERE user_id = %d"
	insertRules = "INSERT INTO rules (user_id, name, parameter) VALUES %s"
	updateRule  = "UPDATE rules SET parameter = %s WHERE user_id = %d and name = %s"
)

type Postgres struct {
	Pool *pgxpool.Pool
}

func NewPostgresRepository(p *pgxpool.Pool) Repository {
	return &Postgres{
		Pool: p,
	}
}

func (p *Postgres) FindAllUserRules(uid int) ([]Rule, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var rows pgx.Rows
	var result []Rule
	var rule Rule

	db.Begin()

	rows, db.Err = db.Conn.Query(context.TODO(), db.Queryf(getAllRules, uid))
	if db.Err != nil {
		db.Rollback()
		return nil, db.Err
	}

	for rows.Next() {
		db.Err = rows.Scan(&rule.ID, &rule.UserID, &rule.Name, &rule.Parameter)
		if db.Err != nil {
			db.Rollback()
			return nil, db.Err
		}

		result = append(result, rule)
	}

	db.Commit()

	return result, nil
}

func (p *Postgres) InsertRule(rule Rule) error {
	db := helpers.NewDatabaseHelper(p.Pool)

	db.Inserter(db.Queryf(insertRules, rule.toSQLString()))
	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()

	return nil
}

func (p *Postgres) InsertRules(rules []Rule) error {
	db := helpers.NewDatabaseHelper(p.Pool)
	var values string

	for _, rule := range rules {
		values += rule.toSQLString() + ", "
	}

	typeString.RemoveLastPattern(", ", &values)

	db.Inserter(db.Queryf(insertRules, values))
	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()

	return nil
}

func (p *Postgres) UpdateRule(rule Rule) error {
	db := helpers.NewDatabaseHelper(p.Pool)

	db.Updater(db.Queryf(updateRule, rule.Parameter, rule.UserID, rule.Name))
	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()
	return nil
}

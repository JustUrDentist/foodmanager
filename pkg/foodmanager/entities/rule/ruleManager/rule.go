package ruleManager

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/food"
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/rule/ruleManager/rules"
)

type FoodRule interface {
	FoodFilter(ff *[]food.Food)
	FoodCombFilter(ffs *[][]food.Food)
	RuleName() string
	RuleState() rules.State
}

type CombRule interface {
}

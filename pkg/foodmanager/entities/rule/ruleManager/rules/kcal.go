package rules

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/food"
)

type Kcal struct {
	Name    string `json:"name"`
	MaxKcal int    `json:"maxKcal"`
	MinKcal int    `json:"minKcal"`
	State   State  `json:"state"`
}

func (cal *Kcal) FoodFilter(foods *[]food.Food) {
	var passedFoods []food.Food
	for _, food := range *foods {
		if food.Kcal <= cal.MaxKcal && food.Kcal >= cal.MinKcal {
			passedFoods = append(passedFoods, food)
		}
	}

	*foods = passedFoods
}

func (cal *Kcal) FoodCombFilter(combFoods *[][]food.Food) {
	var passedCombs [][]food.Food
	for _, comb := range *combFoods {
		for _, food := range comb {
			if food.Kcal >= cal.MaxKcal && food.Kcal <= cal.MinKcal {
				continue
			}
			passedCombs = append(passedCombs, comb)
		}
	}

	*combFoods = passedCombs
}

func (cal *Kcal) RuleName() string {
	return cal.Name
}

func (cal *Kcal) RuleState() State {
	return cal.State
}

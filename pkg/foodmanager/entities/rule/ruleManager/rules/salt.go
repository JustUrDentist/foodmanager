package rules

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/food"
)

type Salt struct {
	Name    string `json:"name"`
	MaxSalt int    `json:"maxSalt"`
	State   State  `json:"state"`
}

func (s *Salt) FoodFilter(foods *[]food.Food) {
	var passedFoods []food.Food
	for _, food := range *foods {
		if food.Salt <= s.MaxSalt {
			passedFoods = append(passedFoods, food)
		}
	}

	*foods = passedFoods
}

func (s *Salt) FoodCombFilter(combFoods *[][]food.Food) {
	var passedCombs [][]food.Food
	for _, comb := range *combFoods {
		for _, food := range comb {
			if food.Salt > s.MaxSalt {
				continue
			}
			passedCombs = append(passedCombs, comb)
		}
	}

	*combFoods = passedCombs
}

func (s *Salt) RuleName() string {
	return s.Name
}

func (s *Salt) RuleState() State {
	return s.State
}

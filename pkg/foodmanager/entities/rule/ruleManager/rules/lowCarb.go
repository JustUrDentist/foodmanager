package rules

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/food"
)

type LowCarb struct {
	Name    string `json:"name"`
	MaxCarb int    `json:"maxCarb"`
	State   State  `json:"state"`
}

func (lc *LowCarb) FoodFilter(foods *[]food.Food) {
	var passedFoods []food.Food
	for _, food := range *foods {
		if food.Carbohydrates <= lc.MaxCarb {
			passedFoods = append(passedFoods, food)
		}
	}

	*foods = passedFoods
}

func (lc *LowCarb) FoodCombFilter(combFoods *[][]food.Food) {
	var passedCombs [][]food.Food
	for _, comb := range *combFoods {
		for _, food := range comb {
			if food.Carbohydrates > lc.MaxCarb {
				continue
			}
			passedCombs = append(passedCombs, comb)
		}
	}

	*combFoods = passedCombs
}

func (lc *LowCarb) RuleName() string {
	return lc.Name
}

func (lc *LowCarb) RuleState() State {
	return lc.State
}

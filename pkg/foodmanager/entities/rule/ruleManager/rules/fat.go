package rules

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/food"
)

type Fat struct {
	Name   string `json:"name"`
	MaxFat int    `json:"maxFat"`
	State  State  `json:"state"`
}

func (f *Fat) FoodFilter(foods *[]food.Food) {
	var passedFoods []food.Food
	for _, food := range *foods {
		if food.Fat <= f.MaxFat {
			passedFoods = append(passedFoods, food)
		}
	}

	*foods = passedFoods
}

func (f *Fat) FoodCombFilter(combFoods *[][]food.Food) {
	var passedCombs [][]food.Food
	for _, comb := range *combFoods {
		for _, food := range comb {
			if food.Fat > f.MaxFat {
				continue
			}
			passedCombs = append(passedCombs, comb)
		}
	}

	*combFoods = passedCombs
}

func (f *Fat) RuleName() string {
	return f.Name
}

func (f *Fat) RuleState() State {
	return f.State
}

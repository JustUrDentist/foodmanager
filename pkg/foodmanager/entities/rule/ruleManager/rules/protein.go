package rules

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/food"
)

type Protein struct {
	Name       string `json:"name"`
	MinProtein int    `json:"minProtein"`
	State      State  `json:"state"`
}

func (p *Protein) FoodFilter(foods *[]food.Food) {
	var passedFoods []food.Food
	for _, food := range *foods {
		if food.Protein >= p.MinProtein {
			passedFoods = append(passedFoods, food)
		}
	}

	*foods = passedFoods
}

func (p *Protein) FoodCombFilter(combFoods *[][]food.Food) {
	var passedCombs [][]food.Food
	for _, comb := range *combFoods {
		for _, food := range comb {
			if food.Protein < p.MinProtein {
				continue
			}
			passedCombs = append(passedCombs, comb)
		}
	}

	*combFoods = passedCombs
}

func (p *Protein) RuleName() string {
	return p.Name
}

func (p *Protein) RuleState() State {
	return p.State
}

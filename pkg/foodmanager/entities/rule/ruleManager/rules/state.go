package rules

type State struct {
	IsCombRule bool `json:"isCombRule"`
	Active     bool `json:"active"`
}

package ruleManager

import (
	"encoding/json"
	"errors"
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/rule"
	"gitlab.com/JustUrDentist/foodmanager/pkg/foodmanager/entities/rule/ruleManager/rules"
)

type FoodRuleManager struct {
	FoodFilters       []FoodRule
	foodCombFilters   []FoodRule
	foodSingleFilters []FoodRule
}

func (frm *FoodRuleManager) FromJson(rules []rule.Rule) error {
	for _, r := range rules {
		rule, err := frm.createRule(r.Name)
		if err != nil {
			return err
		}

		err = json.Unmarshal([]byte(r.Parameter), &rule)
		if err != nil {
			return err
		}

		frm.FoodFilters = append(frm.FoodFilters, rule)
	}

	return nil
}

func (frm *FoodRuleManager) AllRuleNames() []string {
	var result []string
	for _, rule := range frm.FoodFilters {
		result = append(result, rule.RuleName())
	}

	return result
}

func (frm *FoodRuleManager) ToJson() (map[string]string, error) {
	result := make(map[string]string)
	for _, rule := range frm.FoodFilters {
		b, err := json.Marshal(rule)
		if err != nil {
			return nil, err
		}
		result[rule.RuleName()] = string(b)
	}

	return result, nil
}

func (frm *FoodRuleManager) SplitRules() {
	for _, rule := range frm.FoodFilters {
		rState := rule.RuleState()
		if rState.Active {
			if rState.IsCombRule {
				frm.foodCombFilters = append(frm.foodCombFilters, rule)
			} else {
				frm.foodSingleFilters = append(frm.foodSingleFilters, rule)
			}
		}
	}
}

func (frm *FoodRuleManager) createRule(name string) (FoodRule, error) {
	switch name {
	case "Kcal":
		return &rules.Kcal{
			Name:    "Kcal",
			MaxKcal: 0,
			MinKcal: 0,
			State:   rules.State{},
		}, nil
	case "LowCarb":
		return &rules.LowCarb{
			Name:    "LowCarb",
			MaxCarb: 0,
			State:   rules.State{},
		}, nil
	case "Fat":
		return &rules.Fat{
			Name:   "Fat",
			MaxFat: 0,
			State:  rules.State{},
		}, nil
	case "Protein":
		return &rules.Protein{
			Name:       "Protein",
			MinProtein: 0,
			State:      rules.State{},
		}, nil
	case "Salt":
		return &rules.Salt{
			Name:    "Salt",
			MaxSalt: 0,
			State:   rules.State{},
		}, nil
	default:
		return nil, errors.New("rule does not exist")
	}

}

package rule

import "strconv"

type Rule struct {
	ID        int
	UserID    int
	Name      string
	Parameter string
}

func (r *Rule) toSQLString() string {
	return "(" + strconv.Itoa(r.UserID) + ", '" + r.Name + "', '" + r.Parameter + "')"
}

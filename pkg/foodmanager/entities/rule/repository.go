package rule

type Reader interface {
	FindAllUserRules(uid int) ([]Rule, error)
}

type Writer interface {
	InsertRule(rule Rule) error
	InsertRules(rules []Rule) error
	UpdateRule(rule Rule) error
}

type Repository interface {
	Reader
	Writer
}

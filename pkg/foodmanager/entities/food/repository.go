package food

type Reader interface {
	FindAllCategoriesName() ([]string, error)
	FindAll() ([]Food, error)
	FindAllInUserStock(userID int, stockAmount int) ([]Food, error)
	FindAllCategories() (map[string]int, error)
}

type Writer interface {
	InsertFood(f *Food) error
	InsertFoods(foods []Food) error
	InsertCategory(category string) error
	InsertCategories(categories []string) error
}

type Repository interface {
	Reader
	Writer
}

package food

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers/typeString"
	"strconv"
)

const (
	blockQuerySize         = 1000
	getAllFoods            = "SELECT * FROM foods"
	getAllFoodsInUserStock = "SELECT f.* FROM foods f JOIN stocks s ON s.food_id = f.ID WHERE s.user_id = %d AND s.amount >= %d"
	insertFoods            = "INSERT INTO foods (name, kcal, carbohydrates, sugar, salt, fat, protein) VALUES %s RETURNING id"
	insertFoodCategory     = "INSERT INTO food_category (food_id, category_id) VALUES %s"
	getCategories          = "SELECT c.Food FROM Categories JOIN food_category fc ON fc.category_id = c.ID WHERE fc.food_id = %d"
	getCategoriesName      = "SELECT Name FROM Categories"
	getAllCategories       = "SELECT * FROM Categories"
	insertCategory         = "INSERT INTO Categories (name) VALUES %s"
)

type Postgres struct {
	Pool *pgxpool.Pool
}

func NewPostgresRepository(p *pgxpool.Pool) Repository {
	return &Postgres{
		Pool: p,
	}
}

func (p *Postgres) FindAllInUserStock(userID int, stockAmount int) ([]Food, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	return p.selectFoods(db.Queryf(getAllFoodsInUserStock, userID, stockAmount))
}

func (p *Postgres) FindAll() ([]Food, error) {
	return p.selectFoods(getAllFoods)
}

func (p *Postgres) FindAllCategories() (map[string]int, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var rows pgx.Rows
	categories := make(map[string]int)

	db.Begin()
	rows, db.Err = db.Conn.Query(context.TODO(), getAllCategories)

	if db.Err != nil {
		db.Rollback()
		return nil, db.Err
	}

	for rows.Next() {
		var category string
		var id int

		db.Err = rows.Scan(&id, &category)

		if db.Err != nil {
			db.Rollback()
			return nil, db.Err
		}

		categories[category] = id
	}

	db.Commit()

	return categories, nil
}

func (p *Postgres) selectFoods(query string) ([]Food, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var rows pgx.Rows
	var foods []Food
	var name string
	var id, kcal, carbohydrates, sugar, salt, fat, protein int
	var categories []string

	db.Begin()
	defer db.Commit()

	rows, db.Err = db.Conn.Query(context.TODO(), query)

	if db.Err != nil {
		db.Rollback()
		return nil, db.Err
	}

	for rows.Next() {
		db.Err = rows.Scan(&id, &name, &kcal, &carbohydrates, &sugar, &salt, &fat, &protein)
		if db.Err != nil {
			db.Rollback()
			return nil, db.Err
		}
		food := Food{
			ID:            id,
			Name:          name,
			Kcal:          kcal,
			Carbohydrates: carbohydrates,
			Sugar:         sugar,
			Salt:          salt,
			Fat:           fat,
			Protein:       protein,
		}

		foods = append(foods, food)
	}

	for i, food := range foods {
		var category string
		rows, db.Err = db.Conn.Query(context.TODO(), getCategories, food.ID)

		if db.Err != nil {
			db.Rollback()
			return nil, db.Err
		}

		for rows.Next() {
			db.Err = rows.Scan(&category)

			if db.Err != nil {
				db.Rollback()
				return nil, db.Err
			}

			categories = append(categories, category)
		}

		foods[i].Categories = categories
	}

	return foods, nil
}

func (p *Postgres) addFoodCategory(db *helpers.DatabaseHelper, foodIDs []int, foods []Food) error {
	var categories map[string]int
	var values string

	categories, db.Err = p.FindAllCategories()

	if db.Err != nil {
		return db.Err
	}

	for i, fid := range foodIDs {
		for _, category := range foods[i].Categories {
			if categories[category] == 0 {
				return errors.New("not existent category")
			}
			values += "(" + strconv.Itoa(fid) + ", " + strconv.Itoa(categories[category]) + "), "
		}
	}

	_ = typeString.RemoveLastPattern(", ", &values)

	db.Inserter(db.Queryf(insertFoodCategory, values))

	if db.Err != nil {
		return db.Err
	}

	return nil
}

func (p *Postgres) InsertFood(f *Food) error {
	db := helpers.NewDatabaseHelper(p.Pool)
	var id []int

	db.Begin()
	id, db.Err = db.InserterReturnIDs(db.Queryf(insertFoods, f.toSqlString()))

	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	var foods []Food
	foods = append(foods, *f)

	db.Err = p.addFoodCategory(db, id, foods)

	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()

	return nil
}

func (p *Postgres) InsertFoods(foods []Food) error {
	db := helpers.NewDatabaseHelper(p.Pool)
	var ids []int
	divisor := 1

	lenFoods := len(foods)

	for lenFoods/divisor >= blockQuerySize {
		divisor++
		divisor = helpers.NextGreaterDivisor(lenFoods, divisor)
	}

	part := lenFoods / divisor

	for i := 0; i < divisor; i++ {
		var values string

		for _, food := range foods[part*i : part*i+part] {
			values += food.toSqlString() + ", "
		}

		typeString.RemoveLastPattern(", ", &values)

		ids, db.Err = db.InserterReturnIDs(db.Queryf(insertFoods, values))

		if db.Err != nil {
			db.Rollback()
			return db.Err
		}

		db.Err = p.addFoodCategory(db, ids, foods)

		if db.Err != nil {
			db.Rollback()
			return db.Err
		}
	}
	db.Commit()

	return nil
}

func (p *Postgres) InsertCategory(category string) error {
	db := helpers.NewDatabaseHelper(p.Pool)

	db.Inserter(db.Queryf(insertCategory, category))

	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()

	return nil
}

func (p *Postgres) InsertCategories(categories []string) error {
	db := helpers.NewDatabaseHelper(p.Pool)
	var values string

	for _, category := range categories {
		values += "('" + category + "'), "
	}

	db.Err = typeString.RemoveLastPattern(", ", &values)

	if db.Err != nil {
		return db.Err
	}

	db.Inserter(db.Queryf(insertCategory, values))

	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()
	return nil
}

func (p *Postgres) FindAllCategoriesName() ([]string, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var rows pgx.Rows
	var categories []string

	db.Begin()

	rows, db.Err = db.Conn.Query(context.TODO(), getCategoriesName)

	if db.Err != nil {
		db.Rollback()
		return nil, db.Err
	}

	for rows.Next() {
		var category string

		db.Err = rows.Scan(&category)

		if db.Err != nil {
			return nil, db.Err
		}

		categories = append(categories, category)
	}

	db.Commit()

	return categories, nil
}

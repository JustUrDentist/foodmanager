package food

import "strconv"

type Food struct {
	ID            int
	Name          string
	Kcal          int
	Carbohydrates int
	Sugar         int
	Salt          int
	Fat           int
	Protein       int
	Categories    []string
}

func (f *Food) toSqlString() string {
	return "('" + f.Name + "', " + strconv.Itoa(f.Kcal) + ", " + strconv.Itoa(f.Carbohydrates) + ", " + strconv.Itoa(f.Sugar) + ", " + strconv.Itoa(f.Salt) + ", " + strconv.Itoa(f.Fat) + ", " + strconv.Itoa(f.Protein) + ")"
}

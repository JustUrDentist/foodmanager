package user

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers"
	"time"
)

const (
	apiKeyExpire         = time.Minute * 15
	getUser              = "SELECT * FROM users WHERE id = %d"
	getUserWithLoginInfo = "SELECT * FROM users WHERE password = %s AND username = %s OR email = %s"
	existEmail           = "SELECT COUNT(*) FROM users WHERE email = %s"
	existUsername        = "SELECT COUNT(*) FROM users WHERE username = %s"
	getUserIDByAPIKey    = "SELECT id FROM users WHERE api_key = %s"
	updateKeyTime        = "UPDATE users SET api_key_expire = %s WHERE id = %d"
	updateAPIKey         = "UPDATE users SET api_key = %s, api_key_expire = %s WHERE id = %d"
	insertUser           = "INSERT INTO users (username, email, password, daily_kcal, api_key, api_key_expire) VALUES %s"
)

type Postgres struct {
	Pool *pgxpool.Pool
}

func NewPostgresRepository(p *pgxpool.Pool) Repository {
	return &Postgres{
		Pool: p,
	}
}

func (p *Postgres) FindUserInfo(uid int) (User, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var user User

	db.SingleSelect(db.Queryf(getUser, uid), &user.ID, &user.Username, &user.Email, &user.Password, &user.DailyKcal, &user.APIKey, &user.APIKeyExpire)
	if db.Err != nil {
		db.Rollback()
		return user, db.Err
		//return nil, db.Err why is it not possible to return nil instead of user????
	}

	db.Commit()
	return user, nil
}

func (p *Postgres) FindUserByLogin(identifier string, password string) (User, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var user User

	db.SingleSelect(db.Queryf(getUserWithLoginInfo, password, identifier, identifier), &user.ID, &user.Username, &user.Email, &user.Password, &user.DailyKcal, &user.APIKey, &user.APIKeyExpire)
	if db.Err != nil {
		db.Rollback()
		return user, db.Err
		//return nil, db.Err why is it not possible to return nil instead of user????
	}

	db.Commit()
	return user, nil
}

func (p *Postgres) existEntry(query string) (bool, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var count int

	db.SingleSelect(query, &count)
	if db.Err != nil {
		return true, db.Err
	}
	db.Commit()

	return count > 0, nil
}

func (p *Postgres) ExistEmail(email string) (bool, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	return p.existEntry(db.Queryf(existEmail, email))
}

func (p *Postgres) ExistUsername(username string) (bool, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	return p.existEntry(db.Queryf(existUsername, username))
}

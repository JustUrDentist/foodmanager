package user

import (
	"strconv"
	"time"
)

type User struct {
	ID           int
	Username     string
	Email        string
	Password     string
	DailyKcal    int
	APIKey       string
	APIKeyExpire time.Time
}

func (u *User) toSQLString() string {
	return "('" + u.Username + "', '" + u.Email + "', '" + u.Password + "', " + strconv.Itoa(u.DailyKcal) + ", '" + u.APIKey + "', '" + u.APIKeyExpire.String() + "')"
}

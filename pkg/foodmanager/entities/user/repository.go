package user

type Reader interface {
	FindUserInfo(uid int) (User, error)
	FindUserByLogin(identifier string, password string) (User, error)
	ExistEmail(email string) (bool, error)
	ExistUsername(username string) (bool, error)
}

type Writer interface {
}

type Repository interface {
	Reader
	Writer
}

package whitelist

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers/slice"
	"strconv"
)

type Whitelist struct {
	ID          int
	UserID      int
	Combination []int
}

func (wl *Whitelist) toSQLString() string {
	return "(" + strconv.Itoa(wl.UserID) + ", " + slice.IntToSQLString(wl.Combination) + ")"
}

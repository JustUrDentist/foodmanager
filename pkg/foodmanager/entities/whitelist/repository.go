package whitelist

type Reader interface {
	FindAllUserWhitelists(uid int) ([]Whitelist, error)
}

type Writer interface {
	InsertUserWhitelist(bl Whitelist) error
	InsertUserWhitelists(bls []Whitelist) error
}

type Repository interface {
	Reader
	Writer
}

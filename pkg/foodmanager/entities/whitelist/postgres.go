package whitelist

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers"
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers/typeString"
)

const (
	getWhitelists    = "SELECT * FROM whitelists WHERE user_id = %d"
	insertWhitelists = "INSERT INTO whitelists (user_id, combinatinons) VALUES %s"
)

type Postgres struct {
	Pool *pgxpool.Pool
}

func NewPostgresRepository(p *pgxpool.Pool) Repository {
	return &Postgres{
		Pool: p,
	}
}

func (p *Postgres) FindAllUserWhitelists(uid int) ([]Whitelist, error) {
	db := helpers.NewDatabaseHelper(p.Pool)
	var rows pgx.Rows
	var result []Whitelist
	var whitelist Whitelist

	db.Begin()

	rows, db.Err = db.Conn.Query(context.TODO(), db.Queryf(getWhitelists, uid))

	if db.Err != nil {
		db.Rollback()
		return nil, db.Err
	}

	for rows.Next() {
		db.Err = rows.Scan(&whitelist.ID, &whitelist.UserID, &whitelist.Combination)
		if db.Err != nil {
			db.Rollback()
			return nil, db.Err
		}

		result = append(result, whitelist)
	}
	db.Commit()

	return result, nil
}

func (p *Postgres) InsertUserWhitelist(bl Whitelist) error {
	db := helpers.NewDatabaseHelper(p.Pool)

	db.Inserter(db.Queryf(insertWhitelists, bl.toSQLString()))

	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()

	return nil
}

func (p *Postgres) InsertUserWhitelists(bls []Whitelist) error {
	db := helpers.NewDatabaseHelper(p.Pool)
	var values string

	for _, bl := range bls {
		values += bl.toSQLString() + ", "
	}

	typeString.RemoveLastPattern(", ", &values)

	db.Inserter(db.Queryf(insertWhitelists, values))

	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	db.Commit()

	return nil
}

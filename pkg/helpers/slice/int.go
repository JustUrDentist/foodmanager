package slice

import (
	"gitlab.com/JustUrDentist/foodmanager/pkg/helpers/typeString"
	"strconv"
)

func IntToSQLString(a []int) string {
	var result string

	lenArr := len(a)
	result += "'{"
	for i := 0; i < lenArr; i++ {
		result += strconv.Itoa(a[i])
		result += ", "
	}
	typeString.RemoveLastPattern(", ", &result)
	result += "}'"

	return result
}

package typeString

import (
	"errors"
)

func RemoveLastPattern(pattern string, output *string) error {
	lenPat := len(pattern)
	lenOut := len(*output)
	lenDif := lenOut - lenPat
	if lenPat > lenOut {
		return errors.New("output string has less values than pattern string")
	}

	strEnd := (*output)[lenDif:]

	if strEnd != pattern {
		return errors.New("pattern do not match with end of output string")
	}

	*output = (*output)[:lenDif]

	return nil
}

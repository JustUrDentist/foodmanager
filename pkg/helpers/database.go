package helpers

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"time"
)

const (
	DBUrl   = "postgresql://postgres:12345678@localhost:5432/food?pool_max_conns=30"
	timeOut = 120 * time.Second
)

type DatabaseHelper struct {
	Conn  *pgxpool.Conn
	Tx    pgx.Tx
	Err   error
	Pool  *pgxpool.Pool
	inUse bool
}

func NewDatabaseHelper(pool *pgxpool.Pool) *DatabaseHelper {
	return &DatabaseHelper{
		Pool:  pool,
		inUse: false,
	}
}

func (db *DatabaseHelper) AcquireConn() {
	db.Conn = nil

	timeOut := time.Now().Add(timeOut)

	for db.Conn == nil || time.Now().After(timeOut) {
		db.Conn, db.Err = db.Pool.Acquire(context.TODO())
	}
}

func (db *DatabaseHelper) Inserter(sql string) {
	var rows pgx.Rows

	db.Begin()
	rows, db.Err = db.Conn.Query(context.TODO(), sql)

	for rows.Next() {
		db.Err = rows.Scan()
	}
}

func (db *DatabaseHelper) InserterReturnIDs(sql string) ([]int, error) {
	var rows pgx.Rows
	var ids []int

	db.Begin()

	rows, db.Err = db.Conn.Query(context.TODO(), sql)

	if db.Err != nil {
		return nil, db.Err
	}

	for rows.Next() {
		var id int

		db.Err = rows.Scan(&id)

		if db.Err != nil {
			return nil, db.Err
		}

		ids = append(ids, id)
	}

	return ids, nil
}

func (db *DatabaseHelper) Updater(sql string) {
	db.Inserter(sql)
}

func (db *DatabaseHelper) SingleSelect(sql string, output ...interface{}) {
	db.Begin()

	db.Err = db.Conn.QueryRow(context.TODO(), sql).Scan(output...)
}

func (db *DatabaseHelper) MultiSelect(sql string, columns int) {

}

func (db *DatabaseHelper) Query(v ...interface{}) string {
	return fmt.Sprint(v...)
}

func (db *DatabaseHelper) Queryf(sql string, v ...interface{}) string {
	return fmt.Sprintf(sql, v...)
}

func (db *DatabaseHelper) Begin() {
	if !db.inUse {
		db.inUse = true
		db.AcquireConn()
		db.Tx, db.Err = db.Conn.Begin(context.TODO())
	}
}

func (db *DatabaseHelper) Commit() {
	if db.inUse {
		db.Err = db.Tx.Commit(context.TODO())
		db.Conn.Release()
		db.Tx = nil
		db.inUse = false
	}
}

func (db *DatabaseHelper) Rollback() {
	if db.inUse {
		db.Err = db.Tx.Rollback(context.TODO())
		db.Conn.Release()
		db.Tx = nil
		db.inUse = false
	}
}
